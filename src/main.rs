#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]

extern crate dualshock4;
extern crate hidapi;
extern crate serde;
extern crate serialport;

#[macro_use(Serialize, Deserialize)]
extern crate serde_derive;

use std::thread;
use std::sync::mpsc;
use std::time;
use hidapi::{HidApi};

use std::io::{Write};

mod settings;
use settings::Settings;

mod motors;

mod events;
use events::{
    encode_command_center_event,
    decode_soul_event,
    AURIGA_SOUL_EVENT_BUF_SIZE
};

const SERIAL_EVENT_BUF_MAX_SIZE:usize = 1000;

fn main() {
    let mut is_connection_established:bool = false;

    let settings = Settings::new().expect("Unable to load settings file");
    println!("{:?}", settings);

    let ds4_receiver = start_ds4_thread();

    let mut sssettings: serialport::SerialPortSettings = Default::default();
    sssettings.baud_rate = serialport::BaudRate::Baud115200;
    let mut port = serialport::open_with_settings(&settings.auriga.serial_port_name, &sssettings)
        .expect("Unable to open port");

    let mut buf = [0u8; SERIAL_EVENT_BUF_MAX_SIZE];

    loop {
        match port.read(&mut buf[..]) {
            Ok(AURIGA_SOUL_EVENT_BUF_SIZE) => {
                let evt = decode_soul_event(&buf);
                println!("{:?}", evt);
                is_connection_established = true;
            },
            Ok(t) => std::io::stdout().write_all(&buf[..t]).unwrap(),
//            Ok(res) => eprintln!("expecting buffer size to be {:?}, but got {:?}", AURIGA_SOUL_EVENT_BUF_SIZE, res),
            Err(ref err) if err.kind() == std::io::ErrorKind::TimedOut => (),
            Err(err) => eprintln!("{:?}", err)
        }

        if is_connection_established {
            let ds4 = ds4_receiver.recv().unwrap();

            let l2 = ds4.buttons.l2.analog_value.unwrap();
            let r2 = ds4.buttons.r2.analog_value.unwrap();

            let event = events::AurigaCommandCenterEvent {
                serial_event_interval: 2000,
                left_motor_rpm: motors::get_left_motor_rpm(l2, r2, settings.dualshock4.analog_stick_error_margin),
                right_motor_rpm: motors::get_right_motor_rpm(l2, r2, settings.dualshock4.analog_stick_error_margin)
            };

//            println!("{:?}", event);

            let encoded_buf = encode_command_center_event(event);
            port.write(&encoded_buf).expect("unable to write to port");
        }
    }
}

fn start_ds4_thread() -> mpsc::Receiver<dualshock4::Dualshock4Data> {
    println!("start_ds4_thread");

    let (ds4_sender, ds4_receiver) = mpsc::channel();

    thread::spawn(move || {
        let hid_api = HidApi::new().expect("Failed to create HID API instance.");
        let dualshock4_controller = dualshock4::get_device(&hid_api).expect("Failed to open device");

        loop {
            let data = dualshock4::read(&dualshock4_controller).unwrap();
            ds4_sender.send(data).unwrap();
            thread::sleep(time::Duration::from_millis(100));
        }
    });

    ds4_receiver
}
