// TODO 12.03.2018 nviik - Should return struct { left_motor, right_motor }
pub fn get_left_motor_rpm(x: u8, y: u8, error_margin:i16) -> i16 {
    let x0 = clamp_value(x as i16, error_margin);
    let y0 = clamp_value(y as i16, error_margin);

    if x0 < 0 {
        y0 + x0
    } else if x0 > 0 {
        y0 - x0
    } else {
        y0
    }
}

// TODO 12.03.2018 nviik - Should return struct { left_motor, right_motor }
pub fn get_right_motor_rpm(x: u8, y: u8, error_margin:i16) -> i16 {
    let x0 = clamp_value(x as i16, error_margin);
    let y0 = clamp_value(y as i16, error_margin);

    if x0 < 0 {
        y0 - x0
    } else if x0 > 0 {
        y0 + x0
    } else {
        y0
    }
}

fn clamp_value(val: i16, error_margin:i16) -> i16 {
    let min_error_margin:i16 = 128 - error_margin;
    let max_error_margin:i16 = 128 + error_margin;

    if val < min_error_margin || val > max_error_margin {
        val - 128
    } else {
        0
    }
}

#[cfg(test)]
mod tests {
    use self::super::*;

    const TEST_DS4_ANALOG_STICK_ERROR_MARGIN:i16 = 16;

    #[test]
    fn test_clamp_value() {
        assert_eq!(clamp_value(0, TEST_DS4_ANALOG_STICK_ERROR_MARGIN), -128);
        assert_eq!(clamp_value(64, TEST_DS4_ANALOG_STICK_ERROR_MARGIN), -64);
        assert_eq!(clamp_value(127, TEST_DS4_ANALOG_STICK_ERROR_MARGIN), 0);
        assert_eq!(clamp_value(191, TEST_DS4_ANALOG_STICK_ERROR_MARGIN), 63);
        assert_eq!(clamp_value(255, TEST_DS4_ANALOG_STICK_ERROR_MARGIN), 127);
    }

    #[test]
    fn test_when_center() {
        let x = 128;
        let y = 128;

        let left_motor = get_left_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);
        let right_motor = get_right_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);

        assert_eq!(left_motor, 0);
        assert_eq!(right_motor, 0);
    }

    #[test]
    fn test_when_moving_left() {
        let x = 0;
        let y = 128;

        let left_motor = get_left_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);
        let right_motor = get_right_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);

        assert_eq!(left_motor, -128);
        assert_eq!(right_motor, 128);
    }

    #[test]
    fn test_when_moving_up() {
        let x = 128;
        let y = 255;

        let left_motor = get_left_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);
        let right_motor = get_right_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);

        assert_eq!(left_motor, 127);
        assert_eq!(right_motor, 127);
    }

    #[test]
    fn test_when_moving_right() {
        let x = 255;
        let y = 128;

        let left_motor = get_left_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);
        let right_motor = get_right_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);

        assert_eq!(left_motor, -127);
        assert_eq!(right_motor, 127);
    }

    #[test]
    fn test_when_moving_down() {
        let x = 128;
        let y = 0;

        let left_motor = get_left_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);
        let right_motor = get_right_motor_rpm(x, y, TEST_DS4_ANALOG_STICK_ERROR_MARGIN);

        assert_eq!(left_motor, -128);
        assert_eq!(right_motor, -128);
    }
}
