extern crate bincode;

// NOTE 06.03.2018 nviik - Outgoing event to auriga soul
#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct AurigaCommandCenterEvent {
    pub serial_event_interval:u16, // bytes: [0..2]

    // TODO 12.3.2018 nviik - this should actually be i8
    pub left_motor_rpm:i16,
    pub right_motor_rpm:i16
}

pub fn encode_command_center_event(event: AurigaCommandCenterEvent) -> Vec<u8> {
    // TODO 06.03.2018 - Implement proper error handling
    let buf = bincode::serialize(&event).unwrap();
    buf
}

#[cfg(test)]
mod tests {
    use self::super::*;
    use std;

    #[test]
    fn test_encode_command_center_event() {
        let event = AurigaCommandCenterEvent {
            serial_event_interval: 1000,
            left_motor_rpm: 10,
            right_motor_rpm: 20
        };

        let encoded_buf = encode_command_center_event(event);
        let decoded_event = decode_command_center_event(&encoded_buf);

        assert_eq!(encoded_buf.len(), std::mem::size_of::<AurigaCommandCenterEvent>(), "Encoding AurigaCommandCenterEvent failed");
        assert_eq!(event, decoded_event, "Decoding AurigaCommandCenterEvent failed");
    }

    fn decode_command_center_event(buf: &[u8]) -> AurigaCommandCenterEvent {
        let event: AurigaCommandCenterEvent = bincode::deserialize(&buf).unwrap();
        event
    }
}
