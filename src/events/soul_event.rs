extern crate bincode;

// NOTE 06.03.2018 nviik - Incoming event from auriga soul
#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct AurigaSoulEvent {
    pub temperature: f32, // bytes: [0..4]

    pub left_motor_rpm:f32,
    pub right_motor_rpm:f32,

    pub compass_angle:f32
}

// NOTE 05.04.2018 nviik - Using std::mem::size_of may result incorrect output,
//   so buffer size should be updated manually
pub const AURIGA_SOUL_EVENT_BUF_SIZE:usize = 16;

pub fn decode_soul_event(buf: &[u8]) -> AurigaSoulEvent {
    // TODO 06.03.2018 - Implement proper error handling
    let event: AurigaSoulEvent = bincode::deserialize(&buf).unwrap();
    event
}

#[cfg(test)]
mod tests {
    use self::super::*;

    #[test]
    fn test_decode_soul_event() {
        let event: AurigaSoulEvent = AurigaSoulEvent {
            temperature: 32.01,
            left_motor_rpm: 12.0,
            right_motor_rpm: 32.0,
            compass_angle: 360.0
        };

        let encoded_buf = encode_soul_event(event);
        let decoded_event = decode_soul_event(&encoded_buf);

        assert_eq!(encoded_buf.len(), AURIGA_SOUL_EVENT_BUF_SIZE, "Encoding AurigaSoulEvent failed");
        assert_eq!(event, decoded_event, "Decoding AurigaSoulEvent failed");
    }

    fn encode_soul_event(event: AurigaSoulEvent) -> Vec<u8> {
        let buf = bincode::serialize(&event).unwrap();
        buf
    }
}
