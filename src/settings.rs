extern crate config;

use self::config::{Config, ConfigError, File};

#[derive(Debug, Deserialize)]
pub struct Auriga {
    pub serial_port_name:String
}

#[derive(Debug, Deserialize)]
pub struct Dualshock4 {
    pub analog_stick_error_margin:i16
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub auriga: Auriga,
    pub dualshock4: Dualshock4
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut settings = Config::default();
        settings.merge(File::with_name("config/default"))?;
        settings.try_into()
    }
}

#[test]
fn test_default_settings() {
    let settings = Settings::new().unwrap();

    assert_ne!(settings.auriga.serial_port_name.len(), 0, "Serial port name is not defined");
}
